<?php
    echo "Hello BITM";
    echo "<br>";
?>

<%
    echo "This is ASP Tag";
    echo "</br>";
%>

<?
    echo "This is short open tag";
    echo "</br>";
?>

<script language ="php">
        echo " This is script tag ";
</script>


Comments:
  
Comments can be written with three different styles:

1. one line C++ style comment 
2. Multi- line comment\
3. One-line shell style comment


PHP support 'C','C++' and Unix shell style (perl style) comments . For example:
 
<?php
    echo 'This is a test'; // This is a one -line C++ style comment
    /* this is a multi line comments
    yet another line of comments     */
    echo ' This is yet another test';
    echo ' One Final Test';
    # This is a one-line shell-style comment
?>


Types:

PHP support Eight primitive types:
Among them , Four are scaler  types:

1.Boolean
2.Integer
3.Float
4.string


Two Compound types: 
1. Array 
2. Object

Two special types;
1. Resourse
2.NULL


Boolean:
This is the simplest type.
A boolean expresses a truth value . It can be either  TRUE or FALSE.
 

<?php   
    $foo = True; // assign the value TRUE to $foo
?>




Integer:

An integer  is a number of the set Z={...-2,1,2,3}.
 <?php
    echo $a  = 1234; // decimal number
    echo $a = -123; // a negative number
    echo $a = 0123; // octal number
    echo $a = 0X1A; // hexadecimal number
    echo $a = 01111111111/// binary number
    
 ?>



Floating point numbers:



<?php   
    echo $a =1.023;
    echo "</br>";
    echo  $b =1.025;
?>


String:
A string is a series of characters , where a character is the same as a byte.
 A string literal can be spefied in Four diffrent ways:
 1. single Quoted 
 2. double Queted
 3. heredoc syntax
 4. nowdoc syntax 
 
 Example:
  single quoeted:
  <?php

  echo "this is a simple string";
  echo 'You can also have embaded newline';
  ?>

  <?php
    echo "this is a double quoted string ";
  ?>

  <?php
        $str = <<<EOD
                example of string 
                spanning multiple lines 
                using heredoc syntax.
                EOD;
                
                echo $str;
  ?>
 
                <?php
                $str =<<< 'EOD'
                example of string 
                spanning multiple lines
                using nowdoc syntax.
                EOD;
                echo $str;
                  ?>
                
                <?php
                        $array("volvo","BMW","Toyata");
                echo "I like " . $cars[0]. ",".$cars[1]. "and".$cars[]2]. ".";
   ?>